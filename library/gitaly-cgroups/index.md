## Idea/Problem Statement

Noisy-neighbors is a problem we frequently run into for the repository storage nodes (referred to as Gitaly nodes hereafter); a repository or more being requested frequently in a short span of time, usually in the form of git clones, end up consuming the node resources (e.g. CPU and/or memory) and starving other requests from such resources, eventually resulting in a sluggish experience for end-users or even worse, 500 errors.
These repositories are usually several gigabytes in size, and the requests executed upon them can be legit (e.g. a repository got popular suddenly on the internet) or straight-forward abuse.
It is required that such resource-intensive requests be contained in a way that limits their resource usage to ensure other requests are being served without disturbances.
As mentioned earlier, most of these resource-intensive requests are git clones, which are handled by Gitaly by spawning `git upload-pack` processes to handle the clone requests. This presents an opportunity to leverage Linux [cgroups](https://www.kernel.org/doc/html/latest/admin-guide/cgroup-v1/cgroups.html) to run these processes in containers with certain limits imposed on resources such as CPU usage.

## Proposal

At startup time, create a pool of N cgroups (e.g. N=1000), each of which has a budget of P percent of the parent cgroup's budget (e.g. P = 25%). Note that this is intentionally heavily oversubscribed, for reasons explained shortly.

At run time, schedule each `git` process to one of the N cgroups based on a deterministic hash of some repository metadata (e.g. the project id or the full command line). This deterministic selection of cgroup is what provides the desired isolation. Hashing based on a repository's unique id gives the most granular grouping, although grouping by namespace may be a good alternative.

Choosing a large value for N means spreading git repositories thinly among cgroups, such that if one cgroup is saturated, most other repositories will not be, due to being assigned to different cgroups. Effectively today N=1.

Choosing a moderate value for P constrains each cgroup to use a smaller fraction of the resource (memory, CPU, etc.). Effectively today P=100%. The minimum requirements for any one repo depend on some repo-specific properties (e.g. object count and size distribution) and some runtime properties (e.g. concurrently running git processes). Some of these properties are already constrained (e.g. rate-limiting, max repository size) and some are not. So even with P=100% (90 GB), saturation could occur. Choosing a P smaller than 100% gives other cgroups a chance to survive when one cgroup is saturated.

With P = 25% and N = 1000, when any 1 of the N cgroups is saturated, the other cgroups should continue to be close to their normal memory pressure. 99.9% of repos would remain largely unaffected, still sharing the 75% of memory that remains, whereas the 0.1% of repositories in the saturated cgroup would be unavailable as long as the saturation lasts. If a second repository in a different cgroup on that same Gitaly node also saturates its cgroup, then the remaining 50% of memory will be shared by the other 998 cgroups, which depending on workload may start to feel some memory pressure but should still be much more available than without the cgroups' isolation.

## Testing

As of January 2021, Gitaly nodes don't seem to suffer from OOMs, which suggests memory saturation is not an issue currently and thus there is no urgent need to test memory limitation for `git` processes. CPU saturation on the other hand is an issue that we ran into on many occasions.
Targeting a single Gitaly node, we can do some stress testing by concurrently cloning a relatively large repository, such as the Linux kernel or GitLab code base, in large quantity (say, 20 clones) and observing how the node behaves and whether we see an elevated rate of errors or Gitaly requests slowing down. The expectation is we shouldn't run into run into these symptoms with Gitaly's cgroups enabled for the CPU controller.

## Challenges

### Monitoring

We use [cAdvisor](https://github.com/google/cadvisor) for monitoring cgroups on Gitaly nodes. For a single cgroup, it can potentially emit 83 Prometheus metrics. Assuming we configure Gitaly to utilize 100 cgroups, we can be ingesting 8300 metrics per node every minute, or 498000 metrics across 60 Gitaly nodes we currently have in production. This is not a blocker for deployment as Prometheus selects metrics from cAdvisor that matches a regex that we supply. There's also the argument that the cAdvisor metrics are not that important to us as many of them are disk- and network- related and we would only be interested in CPU- and memory-related ones.
